import {IEnvironment} from "./environment.interface";

export const environment: IEnvironment = {
  production: true,
  databaseUri: 'https://todo-app-97b35.firebaseio.com/'
};

import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-flipper',
  templateUrl: './flipper.component.html',
  styleUrls: ['./flipper.component.scss']
})
export class FlipperComponent implements OnInit {
  @Input() triggerByHover = false;
  isFlipped = false;

  constructor() { }

  ngOnInit(): void {
  }

  flip(): void {
    if (this.triggerByHover) {
      return;
    }

    this.isFlipped = !this.isFlipped;
  }

  flipToFront(): void {
    this.isFlipped = false;
  }

  flipToBack(): void {
    this.isFlipped = true;
  }
}

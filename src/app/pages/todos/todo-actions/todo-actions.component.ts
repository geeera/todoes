import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractControl, FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-todo-actions',
  templateUrl: './todo-actions.component.html',
  styleUrls: ['./todo-actions.component.scss']
})
export class TodoActionsComponent implements OnInit {
  @Output() filter = new EventEmitter<{ title: string, status?: boolean }>();
  @Output() deleteAll = new EventEmitter<boolean>();
  @Output() deleteAllCompleted = new EventEmitter<boolean>();
  @Output() markAllAsCompleted = new EventEmitter<boolean>();

  filterForm: FormGroup;

  get filterTitleControl(): AbstractControl {
    return this.filterForm.controls['title'];
  }

  get filterStatusControl(): AbstractControl {
    return this.filterForm.controls['status'];
  }

  constructor() {
    this.filterForm = new FormGroup({
      title: new FormControl(''),
      status: new FormControl('')
    });
  }

  ngOnInit(): void {
    this.filterForm.valueChanges.subscribe((values) => {
      this.filter.emit(values);
    })
  }

}

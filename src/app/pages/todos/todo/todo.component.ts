import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ITodo} from "../../../shared/interfaces/todo.interface";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  @Input() todo!: ITodo;

  @Output() deleteTodo = new EventEmitter<string>();
  @Output() completeTodo = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  onDelete() {
    this.deleteTodo.emit(this.todo.id);
  }

  onComplete() {
    this.completeTodo.emit(this.todo.id);
  }

}

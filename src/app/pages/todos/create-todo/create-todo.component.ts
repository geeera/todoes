import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {ICreateTodo} from "../../../shared/interfaces/todo.interface";

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.scss']
})
export class CreateTodoComponent implements OnInit {
  @Output() createTodo = new EventEmitter<ICreateTodo>()
  form: FormGroup = new FormGroup({});

  get titleControl(): AbstractControl {
    return this.form.controls['title'];
  };

  get descriptionControl(): AbstractControl {
    return this.form.controls['description'];
  };

  get isValid(): boolean {
    return this.form.valid;
  }

  constructor() {}

  ngOnInit(): void {
    this.form = new FormGroup({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
    })
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    this.createTodo.emit(this.form.value);
    this.form.reset();
    this.form.markAsUntouched();
  }
}
